'''This is some trivial code.

Let's change something to trigger a new build.
'''


def my_print(to_be_printed):
    """A custom print function"""
    print(to_be_printed)


if __name__ == "__main__":
    my_print("Hello, world!")
