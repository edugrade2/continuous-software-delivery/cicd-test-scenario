'''Some trivial tests.
'''

from main import my_print

def test_print(capsys):
    """Test case for my_print"""
    my_print("Hello, world!")
    out, _ = capsys.readouterr()
    assert out == "Hello, world!\n"
